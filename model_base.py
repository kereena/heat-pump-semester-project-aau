import os
import datetime
import pandas as pd
import numpy as np
from sklearn.metrics import max_error, mean_absolute_error, mean_squared_error, mean_squared_log_error, \
    median_absolute_error, r2_score

FILE_TEST_SIZE = 1  # the number is % of full size, a variable for testing on smaller sample sets
GPU_ACTIVE = False  # determines if the algorithms are supposed to go GPU or CPU mode

"""
Rolling Window settings for the support vector regression, in order to take the temporal aspect into account.
"""
ROLLING_WINDOW_SVR_ACTIVE = True
ROLLING_WINDOW_SVR_SIZE = 5
ROLLING_WINDOW_SVR_LOOP = True
ROLLING_WINDOW_SVR_LOOP_FLOOR = 2

"""
Rolling Window settings for the linear regression, in order to take the temporal aspect into account.
"""
ROLLING_WINDOW_LR_ACTIVE = True
ROLLING_WINDOW_LR_SIZE = 5
ROLLING_WINDOW_LR_LOOP = True
ROLLING_WINDOW_LR_LOOP_FLOOR = 2

CLF_TYPE = "random"  # Options; "grid", "random" , ""
"""
DATA Granularity, has an influence on what kind of timeframes every model can be run from. And which 
prepared_dataset_{}.csv gets created.
One file gets created for every dictionary entry. 
The accepted parameter input is every dictionary match
To add additional values into the dictionary, key = xmin or xhour, value yT or yH, where T is shorthand for minute and H is shorthand for hour
"""

DATA_ALL_GRANULARITIES = {"15min": "15T", "30min": "30T", "45min": "45T", "hour": "1H", "2hour": "2H"}
DATA_DEFAULT_GRANULARITY = {"2hour": "2H"}

LR_SEP_ACTIVE = True
LR_SEP_SHIFT_FLOOR = 1  # value of i for first iteration
LR_SEP_SHIFT_CEILING = 2  # this - floor is number of iterations in the loop, if ceiling <= floor, then no execution.

"""
In order to execute the composition model, range between LR_SEP_SHIFT_FLOOR , LR_SEP_SHIFT_CEILING need to cover 1,
since LR_COMP is only exectued when j is = 1, and j currently increments on LR_SEP settings.
This means that as long as LR_SEP_SHIFT_FLOOR <= 1, LR_SEP_SHIFT_CEILING >= 2
"""

LR_COMP_ACTIVE = True
LR_COMP_STEP_FLOOR = 1
LR_COMP_STEP_CEILING = 2


def loadfile(period="2hour"):
    """
    :param: the period specifies the filename ending.
    :return: Return a pd.DataFrame of the csv located in the /output/ folder.
    """
    path = "./output/prepared_dataset_{}.csv".format(period)
    return pd.read_csv(path)


def split_data(X, y, trainingSize):
    """
    :param X: a pd.DataFrame of features.
    :param y: a pd.DataFrame of targets.
    :param trainingSize: a float/int with a value 0 < i < 1, is used as %-split between {}_train, {}_test.
    :return: four pd.DataFrames where {}_train is the first part of the split.
    """
    y_train, y_test = np.split(y, [int(trainingSize * len(X))])
    X_train, X_test = np.split(X, [int(trainingSize * len(X))])
    return X_train, X_test, y_train, y_test


# Converts Timestamp string -> datetime[ns], removes rows where [AvgIndoorTemp] > 10
# FILE_TEST_SIZE determines how many % of the full dataframe is used for the run through. 1 = 100%, regular scaling

def preprocess_data(df):
    """

    :param df: a pd.DataFrame in the preprocess phase, supposed to include both feature and target columns.
    :return: a pd.DataFrame, anomaly check and time is rounded to nearest minute and dtype: string -> datetime64[ns].
    """
    print("Preprocessing data")

    if FILE_TEST_SIZE < 0 or FILE_TEST_SIZE > 1:
        size = len(df)
    else:
        size = int(FILE_TEST_SIZE * len(df))
    df = df.head(size)
    df = df.loc[df['AvgIndoorTemp'] > 10]
    df['Timestamp'] = [x[:-6] for x in df['Timestamp']]  # Make Timestamp proper Length
    df['Timestamp'] = pd.to_datetime(df['Timestamp']).dt.round('min')  # Transform dtype from string to datetime64[ns]

    print("Data is preprocessed")
    return df


#
def resample_data(df, timeFrame='1T'):
    """
    Note: Timeframe (H : hours),(T : minutes) - example 3H for 3 hours or 15T for 15 minutes
    :param df: A pd.Dataframe of the features with the intent to work on the data on a different time frame.
    :param timeFrame: The time frame the features get grouped to.
    :return: A pd.Dataframe of the features on the timeFrame time increment and all the values are found by
             taking the mean of the values within the time increment, excluding the timeFrame column.
    """
    print("Resampling data to {}".format(timeFrame))
    df = df.set_index('Timestamp')
    df = df.resample(timeFrame).mean()
    # TODO: Isn't this a hack, I assume the resample should cover both feature and target case?
    # df = df[['SumPower', 'OutdoorTemp', 'AvgIndoorTemp']].reset_index(drop=True)
    return df


def assign_datasets(timeperiod):
    """
    :param timeperiod: Argument for which time period (granularity) one wants.
    :return: A failsafe string. If file found, then returns it, if no file in /output/ has the name,
             file not found, if no argument then default time frame
             specified in the configuration section "DATA_DEFAULT_GRANULARITY"
    """
    print("Timeperiod specified is {}".format(timeperiod))
    if timeperiod == 'all':
        print("Executing every timeframe specified in base.SVR_DEFAULT_GRANULARITY")
        dataset_names = DATA_ALL_GRANULARITIES
    elif os.path.isfile('./output/prepared_dataset_{}.csv'.format(timeperiod)):
        print("File found: Executing {}".format(timeperiod))
        dataset_names = {timeperiod: "found"}
    else:
        print("No time frame found, executing with default: {}".format(next(iter(DATA_DEFAULT_GRANULARITY))))
        dataset_names = DATA_DEFAULT_GRANULARITY
    return dataset_names


def store_data_samples(df, interpolate=True):
    """
    :param df: Takes any pd.DataFrame, intended for: pd.DataFrame of the csv located in the /output/ folder.
    :param interpolate: Fills in missing data based on the specified settings. To read more about options and how they
            work, visit: https://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html
    :return: Void, writes the interpolated pd.DataFrames to that satisfy the "DATA_ALL_GRANULARITIES" values to csv file
            in the directory /output/ filename prepared_dataset_{}.csv with the "DATA_ALL_GRANULARITIES" key names in {}
    """
    timeintervals = DATA_ALL_GRANULARITIES
    df = preprocess_data(df)
    if interpolate:
        print("Interpolating data with linear model on minute dataset")
        df = df.interpolate(downcast='infer', method='linear')
        print("Interpolation done")
    for key, value in timeintervals.items():
        df2 = resample_data(df, value).copy()
        path = "output/prepared_dataset_{}.csv".format(key)
        df2.to_csv(path, index=True, header=True)


def estimators_print(y_true, y_pred):
    """
    :param y_true: A pd.DataFrame of targets
    :param y_pred: A pd.Dataframe of predicted targets
    :return: Void: prints several different error measures. Source for metric equations and descriptions
    https://scikit-learn.org/stable/modules/model_evaluation.html#regression-metrics
    """
    """Max Error, captures the worst case error between the predicted and true value. Perfect prediction when = 0"""
    print("Max error: %.2f" % max_error(y_true, y_pred))

    """Mean absolute error MAE"""
    print("Mean absolute error: %.2f" % mean_absolute_error(y_true, y_pred))

    """The mean squared error MSE"""
    print("Mean squared error: %.2f"
          % mean_squared_error(y_true, y_pred))

    """The mean squared log error MSLE"""
    print("The mean squared log error: %.2f" % mean_squared_log_error(y_true, y_pred))

    """The median absolute error MedAE"""
    print("The median absolute error: %.2f" % median_absolute_error(y_true, y_pred))

    """Explain varience score: 1 is perfect prediction"""
    print("Varience score: %.2f" % r2_score(y_true, y_pred))


def generate_estimator_row(y_true, y_pred, name):
    """
    :param y_true: A pd.DataFrame of targets
    :param y_pred: A pd.Dataframe of predicted targets
    :param name: The model name
    :return: A one row pd.DataFrame (series) of several error measures for the difference between target and prediction
    """
    print(y_true)
    print(y_pred)
    maxE = max_error(y_true, y_pred)
    MAE = mean_absolute_error(y_true, y_pred)
    MSE = mean_squared_error(y_true, y_pred)
    MSLE = mean_squared_log_error(y_true, y_pred)
    MedAE = median_absolute_error(y_true, y_pred)
    R2 = r2_score(y_true, y_pred)

    df = pd.DataFrame({'Name': [name], 'MAXE': [maxE], 'MAE': [MAE], 'MSE': [MSE],
                       'MSLE': [MSLE], 'MedAE': [MedAE], 'R2': [R2]})
    df.set_index("Name", inplace=True)
    return df


def timestamp():
    """ :return: A timestamp format:YYYYmmdd_HH-MM-SS """
    now = datetime.datetime.now()
    return str(now.strftime("%Y%m%d_%H-%M-%S"))


def rolling_config(X, y, rolling):
    """
    :param X: A pd.DataFrame of features
    :param y: A pd.DataFrame of target
    :param rolling: How large the rolling time window is going to be
    :return: The pd.DataFrame of features (X) from (T-rolling) up to T, in rows. The pd.DataFrame of targets (y) is a
    offset of rolling from the input.
    """
    X = pd.DataFrame.from_records(array_matrix(X, rolling))
    X = X.iloc[:-1]
    y = y.iloc[rolling:]
    return X, y


# Array_matrix, Take_array are methods for the rolling window time frame,
def array_matrix(df, rolling):
    """
    Helper function for rolling_config.
    :param df: A pd.DataFrame of features
    :param rolling: For all values in range(ROLLING_WINDOW_LOOP_FLOOR, ROLLING_WINDOW_SIZE).
    :return: A np.ndarray of features (X) from (T-rolling) up to T
    """
    myNpArray = np.zeros([1, rolling * 3])
    rollinglist = take_array(rolling)
    for i in range(len(df) - (rolling - 1)):
        rollingwindow = [x + i for x in rollinglist]
        lst = df.take(rollingwindow).values.flatten()
        myNpArray = np.vstack((myNpArray, lst))
    return myNpArray[1:]


def take_array(rolling):
    """
    Helper function for array_matrix.
    :param rolling: For all values in range(ROLLING_WINDOW_LOOP_FLOOR, ROLLING_WINDOW_SIZE).
    :return: A list of all the values in the incrementer from the :param rolling
    """
    result = []
    for i in range(rolling):
        result.append(i)
    return result


def create_folders(basepath, extensions=None):
    """
    :param basepath: The base folder (not from root, but from local path)
    :param extensions: Every sub-folder that is going to be created under the base folder
    :return: Void.
    """
    if extensions is None:
        extensions = []
    if not len(extensions) == 0:
        for item in extensions:
            path = basepath+item
            if not os.path.exists(path):
                os.makedirs(path)
    else:
        if not os.path.exists(basepath):
            os.makedirs(basepath)
