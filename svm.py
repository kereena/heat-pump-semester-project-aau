import sys
import numpy as np
from sklearn.svm import SVR
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn import preprocessing
import scipy.stats
import pandas as pd
import model_base as base

"""" 
Warning, only import thundersvm if it is properly installed, pip install does not work.
Remember to comment out from sklearn.svm import SVR when activating thundersvm.
"""
### NOT WORKING AFTER REFACTOR
#from thundersvm import SVR
if "thundersvm" in sys.modules:
    GPU_ACTIVE = True

#TODO:Add error rate pd.DataFrame generator to support_vector_regression


def support_vector_regression(timeperiod="default"):
    """
    :param timeperiod: Argument for which time period (granularity) one wants.
    :return: Void. Controls the pipeline for SVR
    """
    dataset_names = base.assign_datasets(timeperiod)

    for key, value in dataset_names.items():
        # Load data
        filename = key
        df = base.loadfile(filename)

        #TODO. figure out why the files don't get resampled properly.
        X = df[['SumPower', 'OutdoorTemp', 'AvgIndoorTemp']].fillna(method='bfill')
        y = df[['AvgIndoorTemp']].fillna(method='bfill')

        # Size of the rolling window, from t to t + rolling value
        if base.ROLLING_WINDOW_SVR_ACTIVE:
            if base.ROLLING_WINDOW_SVR_LOOP:
                for rolling in range(base.ROLLING_WINDOW_SVR_LOOP_FLOOR, base.ROLLING_WINDOW_SVR_SIZE):
                    X_roll, y_roll = base.rolling_config(X, y, rolling)
                    support_vector_regression_helper(X_roll, y_roll)
            else:
                rolling = base.ROLLING_WINDOW_SVR_SIZE
                X, y = base.rolling_config(X, y, rolling)
                support_vector_regression_helper(X, y)
        else:
            support_vector_regression_helper(X, y)


def support_vector_regression_helper(X, y):
        """
        :param X: A pd.DataFrame of features
        :param y: A pd.DataFrame of target
        :return: Void. prints the configuration of the best model
        """

        X_train, X_test, y_train, y_test = base.split_data(X, y, 0.8)
        X_test_S = preprocessing.scale(X_test)

        clf = assign_clf(base.CLF_TYPE)

        """train the model, if type() not a np.ndarray then use .values.ravel() on the data frame"""
        print(y_test.head())
        X_fit, y_fit = fit_type_safe(X_test_S, y_test)
        clf.fit(X_fit, y_fit)

        if base.CLF_TYPE == "grid" or base.CLF_TYPE == "random":
            """print the best parameters found in the hyper parameter optimization"""
            best_params = best_performance(clf, verbose=False)
        else:
            """Accuracy score from last model training"""
            best_params = clf.score(X_test_S, y_test)

        #Optional: Print predicted values
        #pred = clf.predict(X_test_S)
        #print(pred)
        print(best_params)


def fit_type_safe(X, y):
    """
    To read more about ravel source below:
    https://docs.scipy.org/doc/numpy/reference/generated/numpy.ravel.html
    :param X: A collection of features. Gets converted the to np.ndarray if it is any other type.
    :param y: A collection of targets. Gets converted the to np.ndarray if it is any other type.
    :return: X is a np.ndarray and y is an array of the same subtype as a, with shape (a.size,)
    """
    if not isinstance(X, np.ndarray):
        X = X.to_numpy()

    if not isinstance(y, np.ndarray):
        y = y.to_numpy().ravel()
    else:
        y = y.ravel()
    return X, y


def assign_clf(clftype):
    """
    :param clftype: grid search is a hyper parameter optimization technique, it looks at all the combinations on the
           predefined value-set. To find the combination that returns the best result
           (model with most accurate predictive power)
           randomized_search is a hyper parameter optimization technique, it takes some random numbers from
           a specified function. Then loops the training for n_iter times, afterwards it returns the best result
           (model with most accurate predictive power)
    :return:
    """
    if clftype == "grid":
        clf = grid_search()
    elif clftype == "random":
        clf = randomized_search()
    else:
        """Different kind of kernels. Use them if hyper parameter optimization is not a concern. (Fastest usage)"""
        #clf = SVR(kernel='linear',gamma='scale')
        #clf = SVR(kernel='poly', gamma='scale')
        #clf = SVR(kernel='sigmoid', gamma='scale')
        #clf = SVR(kernel='rbf', gamma='auto', C=100, epsilon=0.5)
        clf = SVR(epsilon=0.3, C=100, kernel='rbf')
    return clf


def grid_search():
    """
    grid search is a hyper parameter optimization technique, it looks at all the combinations on the
    predefined value-set. To find the combination that returns the best result
    (model with most accurate predictive power).
    :return: A collection of trained models within of all the combinations within tuned_parameters.
    """
    #tuned_parameters = [{'epsilon': [0.01, 0.1, 0.3, 0.5, 1], 'C': [0.1, 1, 5, 10, 50, 100, 500, 1000], 'kernel': ['rbf']}]
    tuned_parameters = [{'epsilon': [0.3, 0.5], 'C': [1, 10, 100], 'kernel': ['rbf']}]
    if base.GPU_ACTIVE:
        return GridSearchCV(SVR(), tuned_parameters, cv=5, n_jobs=-1)
    else:
        return GridSearchCV(SVR(gamma="scale"), tuned_parameters, cv=5, n_jobs=1, scoring='r2')


def randomized_search():
    """
    randomized_search is a hyper parameter optimization technique, it takes some random numbers from
    a specified function. Then loops the training for n_iter times, afterwards it returns the best result
    (model with most accurate predictive power).
    :return: A collection of trained models within the bounded random criteria.
    """
    tuned_parameters = {'epsilon': scipy.stats.expon(scale=0.5), 'C': scipy.stats.expon(scale=100), 'kernel': ['rbf']}
    if base.GPU_ACTIVE:
        return RandomizedSearchCV(SVR(), tuned_parameters, cv=5, n_jobs=-1, n_iter=10)
    else:
        return RandomizedSearchCV(SVR(gamma="scale"), tuned_parameters, cv=5, n_jobs=-1, n_iter=10)


def print_performance(clf):
    """
    :param clf: The instance of the Support Vector Regression.
    :return: Void, Prints the hyper parameter optimization trainings stack, results printed is the settings and score.
    """
    for param, score in zip(clf.cv_results_['params'], clf.cv_results_['mean_test_score']):
        print(param, score)


def best_performance(clf, verbose=False):
    """
    :param clf: The instance of the Support Vector Regression.
    :param verbose: Controls if the print_performance() method is called.
    :return: The best hyper parameter configuration of the model
    """
    if verbose:
        print_performance(clf)
    return clf.best_params_


def print_predict(clf, x_test, y_test):
    """
    :param clf: The instance of the Support Vector Regression.
    :param x_test: Test-split of the features in order to generate the predicted values.
    :param y_test: Test-split of the targets, to compare against the predicted values.
    :return: Void. Prints a table of target values, predicted values and the difference between them in % in abs-value.
    """
    pred = clf.predict(x_test)
    for i in range(pred.size):
        print("%f %f = %.2f %s" % (y_test[i], pred[i], (np.abs(1-(y_test[i]/pred[i]))*100), "%"))