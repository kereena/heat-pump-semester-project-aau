import dataPrep as dpr
import visualizer as vis
import download_dataset
import os
import argparse
from linear_regression import linear_regression
from svm import support_vector_regression
import time
import model_base as base

# Returns separate dataframes for each original dataset
def get_separate_frames():
    return (dpr.open_hvac_data(), dpr.open_indenv_data(), dpr.open_outenv_data())

# Prepares a 'final' dataset (only containing necessary information)
def get_final_dataset():
    indenv_data = dpr.open_indenv_data()
    outenv_data = dpr.open_outenv_data()
    hvac_data = dpr.open_hvac_data()
    prepped = dpr.get_prepared_dataset(hvac_data, outenv_data, indenv_data)
    return prepped

# Saves 'final' dataset
def save_final_dataset(input_dataset_dir, postfix, output_dataset_file):
    def dataset_filename(prefix):
        return os.path.join(input_dataset_dir, "%s-%s.csv" % (prefix, postfix))

    indenv_data = dpr.open_indenv_data(dataset_filename("IndEnv"))
    outenv_data = dpr.open_outenv_data(dataset_filename("OutEnv"))
    hvac_data = dpr.open_hvac_data(dataset_filename("HVAC"))
    dpr.save_prepared_dataset(hvac_data, outenv_data, indenv_data, output_dataset_file)

# This performs vizualizations
# Vizualizes overall temperature (indoor AND outdoor) and power usage
# As well as various indoor temperature readings (for diff. rooms)
def test_pipeline():
    data = get_final_dataset()[0]
    temps = get_separate_frames()[1]
    #vis.visualize_data(data,temps)
    print(data.head(20))


def prepared_dataset_filename(dataset_postfix):
    return os.path.join("output", "prepared_dataset_%s.csv" % (dataset_postfix,))


def do_prepare_directory(dataset_postfix):
    print("Making datasets available on laptop...")
    download_dir = "site-data"
    dump_dir = "images/TemporaryDump"
    base.create_folders(download_dir)
    base.create_folders(dump_dir, ["/pdf/Annotated", "/pdf/Clean", "/png/Annotated", "/png/Clean"])
    do_prepare_dataset(dataset_postfix, download_dir)


def do_prepare_dataset(dataset_postfix, download_dir):
    if dataset_postfix == "all":
        prepared_dataset_file = prepared_dataset_filename("minute")
    else:
        prepared_dataset_file = prepared_dataset_filename(dataset_postfix)
    print("Saving final dataset to", prepared_dataset_file)

    #Download files
    if not os.path.isfile(prepared_dataset_file):
        download_dataset.download(download_dir)
        save_final_dataset(download_dir, dataset_postfix, prepared_dataset_file)

    if dataset_postfix == "all":
        base.store_data_samples(base.loadfile("minute"))
    print(dataset_postfix)


def do_linear_regression(dataset_postfix):
    #prepared_dataset_file = prepared_dataset_filename(dataset_postfix)
    print("Running linear regression")
    linear_regression(dataset_postfix)


def do_svr(dataset_postfix):
    #prepared_dataset_file = prepared_dataset_filename(dataset_postfix)
    print("Running support vector regression...")
    start = time.time()
    #support_vector_regression(dataframe, premade=True, rollingwindow=False)
    #support_vector_regression(prepared_dataset_file, dataset_postfix, True)
    support_vector_regression(dataset_postfix)
    end = time.time()
    print("elapsed time: ", end - start)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--prepare-dataset", help="Prepare hour or minute based dataset (argument is hour or minute)")
    parser.add_argument("--linear-regression", help="Run linear regression on a prepared dataset (arguments 15min, 30min, 45min, hour, 2hour, all)")
    parser.add_argument("--svr", help="Run SVR (arguments 15min, 30min, 45min, hour, 2hour, all)")
    #TODO add these arguments to linear-regression
    args = parser.parse_args()
    if args.prepare_dataset:
        do_prepare_directory(args.prepare_dataset)
    elif args.linear_regression:
        do_linear_regression(args.linear_regression)
    elif args.svr:
        do_svr(args.svr)
    else:
        parser.usage()
