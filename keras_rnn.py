import math
import os

import numpy as np

import pandas as pd

import sklearn as sk
from sklearn import preprocessing as skpp

import matplotlib as mpl
import matplotlib.pyplot as plt
import tensorflow as tf

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM, SimpleRNN, GRU #NOTE: other RNN impls are imported here as well
from keras.preprocessing.sequence import TimeseriesGenerator
import keras

import model_base as base

import traceback
import timeit

ONE_DAY_IN_MINUTES = 24 * 60

def calculate_sample_count(sampling_size_in_minutes, total_days, training_days):
    assert total_days > training_days
    total_data_sample_count = int(total_days * ONE_DAY_IN_MINUTES / sampling_size_in_minutes)
    training_data_sample_count = int(training_days * ONE_DAY_IN_MINUTES / sampling_size_in_minutes)
    return (total_data_sample_count, training_data_sample_count)

default_opts = {}
# -- Data set options
default_opts['SAMPLING_SIZE_FILE_NAME'] = 'hour'
default_opts['SAMPLING_SIZE_IN_MINUTES'] = 60
default_opts['TOTAL_DATA_SAMPLE_COUNT'] = int(28 * ONE_DAY_IN_MINUTES / default_opts['SAMPLING_SIZE_IN_MINUTES'])
default_opts['TRAINING_SAMPLE_COUNT'] = int(default_opts['TOTAL_DATA_SAMPLE_COUNT'] - (4 * ONE_DAY_IN_MINUTES / default_opts['SAMPLING_SIZE_IN_MINUTES']))

# -- Model options
default_opts['RNN_TYPE'] = 'LSTM'
# TODO: according to documentation this defines the dimensionality of the output space
# If that is true LSTM_UNITS = DENSE_LAYER_UNITS
default_opts['RECURRENT_UNIT_COUNT'] = 1
# LSTM 'Activation' function (TODO: check if only used for candidate long-term mem computation)
default_opts['ACTIVATION_FUNCTION'] = 'relu' # 'tanh' - classical one.
default_opts['RECURRENT_ACTIVATION_FUNCTION'] = 'hard_sigmoid' # (TODO: check if used for everything else) #TODO: what is hard_sigmoid
default_opts['DENSE_LAYER_UNIT_COUNT'] = 1 #TODO: experiment
default_opts['OPTIMIZER'] = 'adam'
default_opts['LOSS_FUNCTION'] = 'mse' # NOTE: rmse isn't implemented in keras? see if we could plug scikit-learn or smth
# STEPS_PER_EPOCH = 1 #TODO: find out exactly what this does
default_opts['EPOCHS'] = 1000
default_opts['BATCH_SIZE'] = 1000 # This will need to be higher than 1 for proper training
default_opts['PAST_TIME_STEP_COUNT'] = 25 # Rolling timeframe (time-window) /// how many rows input has
default_opts['EPOCH_SAVE_INTERVAL'] = 200

# -- Other options
default_opts['VERBOSE_MODE'] = 0 # 1 - yes, 0 - no

def get_default_opts():
    return default_opts

def build_dataframe(opts):
    # Load datafile with the selected sampling size
    sampling_size_file_name = opts['SAMPLING_SIZE_FILE_NAME']
    total_count = opts['TOTAL_DATA_SAMPLE_COUNT']
    training_sample_count = opts['TRAINING_SAMPLE_COUNT']
    df = base.loadfile(sampling_size_file_name)

    # Take the first TOTAL_SAMPLE_DATA_COUNT lines
    total = df.head(total_count)
    total = total.interpolate() #TODO: DIRTY HACK TO ACTUALLY REMOVE NaNs
    training = total.head(training_sample_count)
    testing = total.tail(total_count - training_sample_count)
    return (training, testing)

def build_splits(train_df, test_df):
    input_columns = ['SumPower', 'OutdoorTemp', 'AvgIndoorTemp']
    output_columns = ['AvgIndoorTemp']

    train_X = train_df[input_columns]
    train_y = train_df[output_columns]
    test_X = test_df[input_columns]
    test_y = test_df[output_columns]
    return (train_X, train_y, test_X, test_y)

# LINK TO ORIGINAL: https://stackoverflow.com/questions/54323960/save-keras-model-at-specific-epochs
class EpochSaver(keras.callbacks.Callback):

    def __init__(self, epoch_save_interval, file_name_prefix):
        super().__init__()
        self._epoch_save_interval = epoch_save_interval
        self._file_name_prefix = file_name_prefix

    def on_epoch_end(self, epoch, logs={}):
        if epoch % self._epoch_save_interval == 0 and epoch > 0:  # or save after some epoch, each k-th epoch etc.
            self.model.save("{}_epoch-{}.hd5".format(self._file_name_prefix, epoch))

def run_experiment(epoch_save_file_name_prefix, opts = default_opts):
    train_df, test_df = build_dataframe(opts)
    train_X, train_y, test_X, test_y = build_splits(train_df, test_df)

    tf.reset_default_graph()  # TODO: check if this is necessary
    keras.backend.clear_session()

    # NOTE:
    # We're going to use a TimeseriesGenerator for the purposes creating batches of data to train on
    # It splits input dataset into 'length' size (in terms of rows) chunks
    # Also, you can input it directly to a Keras built model
    training_ts_gen = TimeseriesGenerator(train_X.values, train_y.values, opts['PAST_TIME_STEP_COUNT'],
                                          batch_size=opts['BATCH_SIZE'])
    first_x, first_y = training_ts_gen[0]
    number_of_timesteps_in_sample = first_x.shape[1]
    number_of_features_in_sample = first_x.shape[2]

    # ------------------------------------------------
    # [[[[[[[ ------- MODEL DEFINITION -------- ]]]]]]]
    # ------------------------------------------------

    rnn_types = {}
    rnn_types['LSTM'] = LSTM(units=opts['RECURRENT_UNIT_COUNT'], activation=opts['ACTIVATION_FUNCTION'],
                             recurrent_activation=opts['RECURRENT_ACTIVATION_FUNCTION'],
                             input_shape=(number_of_timesteps_in_sample, number_of_features_in_sample))
    rnn_types['SIMPLE_RNN'] = SimpleRNN(units=opts['RECURRENT_UNIT_COUNT'],
                                        activation=opts['ACTIVATION_FUNCTION'],
                                        input_shape=(number_of_timesteps_in_sample, number_of_features_in_sample))
    rnn_types['GRU'] = GRU(units=opts['RECURRENT_UNIT_COUNT'], activation=opts['ACTIVATION_FUNCTION'],
                           recurrent_activation=opts['RECURRENT_ACTIVATION_FUNCTION'],
                           input_shape=(number_of_timesteps_in_sample, number_of_features_in_sample))

    epoch_save_interval = opts['EPOCH_SAVE_INTERVAL']

    model = Sequential()
    model.add(rnn_types[opts['RNN_TYPE']])
    model.add(Dense(units=opts['DENSE_LAYER_UNIT_COUNT']))
    model.compile(optimizer=opts['OPTIMIZER'], loss=opts['LOSS_FUNCTION'])
    # fit model
    epoch_saver_callback = EpochSaver(epoch_save_interval, epoch_save_file_name_prefix)
    model.fit_generator(training_ts_gen, epochs=opts['EPOCHS'], verbose=opts['VERBOSE_MODE'],callbacks=[epoch_saver_callback])

    # ------------------------------------------------
    # [[[[[[[ ------- Testing ------- ]]]]]]]
    # ------------------------------------------------

    testing_ts_gen = TimeseriesGenerator(test_X.values, test_y.values, opts['PAST_TIME_STEP_COUNT'],
                                         batch_size=opts['BATCH_SIZE'])
    predictions = model.predict_generator(testing_ts_gen, verbose=opts['VERBOSE_MODE'])
    return (model, predictions)

# NOTE: this is just for testing to see if the dateframes are constructed reasonably
# def test_data_frame():
#     opts = get_default_opts()
#     opts['TOTAL_DATA_SAMPLE_COUNT'], opts['TRAINING_SAMPLE_COUNT'] = \
#         calculate_sample_count(sampling_size_in_minutes=60, total_days=5, training_days=5 - 2)
#     opts['SAMPLING_SIZE_FILE_NAME'] = 'hour'
#     opts['SAMPLING_SIZE_IN_MINUTES'] = 60
#     opts['BATCH_SIZE'] = 1  # TODO: <<<
#     opts['EPOCHS'] = 3  # TODO: <<<
#     both_dfs = build_dataframe(opts)
#     splits = build_splits(both_dfs[0], both_dfs[1])
#     return opts, both_dfs, splits

def run_multiple_experiments():
    print("RUNNING RNN EXPERIMENT... ")
    opts = get_default_opts()
    print("Default opts: {}\n".format(opts))
    print("------------------------------------")

    opts['EPOCHS'] = 2001
    opts['EPOCH_SAVE_INTERVAL'] = 200

    for file_name, minutes in [('15min', 15), ('30min', 30), ('hour', 60)]:
        opts['TOTAL_DATA_SAMPLE_COUNT'], opts['TRAINING_SAMPLE_COUNT'] = \
            calculate_sample_count(minutes, 180, 180-7)
        # opts['TOTAL_DATA_SAMPLE_COUNT'], opts['TRAINING_SAMPLE_COUNT'] = \
        #     calculate_sample_count(sampling_size_in_minutes=60, total_days=5, training_days=5-2)
        for algo in ['LSTM', 'GRU', 'SIMPLE_RNN']:
            for memory in [7, 14, 28]:
                for batch_size in [2500, 5000, 10000]:
                    output_file_name_prefix = 'output/saved-trained-models/half_year_model-{}_step-{}_loss-{}_batch-{}_mem-{}'\
                        .format(algo, file_name, opts['LOSS_FUNCTION'], batch_size, memory)
                    try:
                        opts['SAMPLING_SIZE_FILE_NAME'] = file_name
                        opts['SAMPLING_SIZE_IN_MINUTES'] = minutes
                        opts['RNN_TYPE'] = algo
                        opts['PAST_TIME_STEP_COUNT'] = memory

                        # opts['SAMPLING_SIZE_FILE_NAME'] = 'hour'
                        # opts['SAMPLING_SIZE_IN_MINUTES'] = 60
                        # opts['BATCH_SIZE'] = 1 #TODO: <<<
                        # opts['EPOCHS'] = 3 #TODO: <<<
                        print(opts)
                        start = timeit.default_timer()
                        model, predictions = run_experiment(output_file_name_prefix, opts)
                        stop = timeit.default_timer()
                        print("Time: {}".format(stop-start))
                    except Exception as e:
                        text_file = open(output_file_name_prefix + ".txt", "w")
                        error_msg = "Error: {}\n\n".format(str(e))
                        print(error_msg)
                        text_file.write(error_msg)
                        text_file.write("Opts: {}".format(str(opts)))
                        text_file.write("\n")
                        text_file.write(traceback.format_exc())
                        text_file.close()

run_multiple_experiments()
